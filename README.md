# Block Blocker
Found at: https://www.curseforge.com/minecraft/mc-mods/block-blocker

## Description
Join at least [![](https://img.shields.io/discord/96753964485181440.svg?colorB=7289DA)](https://discord.gg/jMvUW6V) of us on Discord right now!

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

There are a few cases in the course of modding history where it is evident that a mechanism is required to prevent the breaking or placing of blocks into the Minecraft world. This mod provides a configuration that allows a server administrator to specify the blocks that they do not want to be breakable nor to be placed.

For example:

In some versions of Ice and Fire there exists a pixie house that is world gen'd in and when it is broken the client and server both crash. To prevent this you would add the ID for the pixie house to the configuration and restart your server. Now no one can break the block and crash the server.

There may be other cases where you would want this behavior.

Another example:

You have a hard mode pack and you want to make it harder, therefore you add other blocks to the list to prevent them from being broken much like bedrock. Stone, Gravel, Sand could be some.

Similarly why stop there, maybe there are certain blocks in your world that have crafting uses but you don't want to see them placed in your world. For instance you don't want people building dirt towers or lava sources. You would add those ID's to the prevent\-placement section of the configuration and restart your server.

The mod is not required on the client but can exist and run there as well, which is great for LAN and Hamachi play too.

The following example config is the config that generates when you first run the mod, this way you can prepopulate the config the with blocks you want to block for placing and harvesting.

blockblocker.cfg

## Notes

Does not block fluid placement. There is an excellent mod that does that already found at [https://www.curseforge.com/minecraft/mc\-mods/no\-lava\-bucketing](https://www.curseforge.com/minecraft/mc-mods/no-lava-bucketing)

Now ported to 1.7.10

None of the versions support title entities or nbt data. Possibly planned, but can't say when, a fat donation of higher than $20 at [https://patreon.com/kreezxil](https://patreon.com/kreezxil) can speed up this process. Make sure you send me a message via private what your donation email was and what it was for.

## Modpacks

No need to ask, just add. A link back not required but sure would be sweet.

## Reviewers

Go ahead. Let me know and I'll feature it. I only ask that you link back to this page in your review description.

## Help a Veteran Today

I am US Veteran and make these packs and mods for you guys. I am not disabled but would appreciate additional funding with your donations at [https://patreon.com/kreezxil](https://patreon.com/kreezxil) .

**This project is proudly powered by FORGE**, without whom it would not be possible. Help FORGE get rid of Adfocus at [https://www.patreon.com/LexManos](https://www.patreon.com/LexManos).